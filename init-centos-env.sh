hostname=$1

#init_host='https://waykichain.oss-cn-shenzhen.aliyuncs.com/devops/a_init_sys.sh'
#init_user='https://waykichain.oss-cn-shenzhen.aliyuncs.com/devops/b_init_user.sh'
#init_docker='https://waykichain.oss-cn-shenzhen.aliyuncs.com/devops/c_init_docker.sh'

init_host='https://gitlab.com/nchain-public/devops/-/raw/master/host-deploy/system-init.sh'
init_user='https://gitlab.com/nchain-public/devops/-/raw/master/host-deploy/user-init.sh'
init_docker='https://gitlab.com/nchain-public/devops/-/raw/master/host-deploy/docker-init.sh'

hidden_zshrc='https://gitlab.com/nchain-public/devops/-/raw/master/host-deploy/hidden.zshrc'
mytheme='https://gitlab.com/nchain-public/devops/raw/master/mytheme.zsh-theme'

wget $hidden_zshrc -O /tmp/.zshrc
wget $mytheme -O /tmp/mytheme.zsh-theme
useradd -m richardchen && chown richardchen. /tmp/mytheme.zsh-theme && chown richardchen. /tmp/.zshrc

curl $init_host   | sudo sh -s $hostname
curl $init_user   | sudo sh
curl $init_docker | sudo sh

sudo curl -L https://github.com/docker/compose/releases/download/v2.5.0/docker-compose-linux-x86_64 -o/usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
