SW_URL='https://waykichain.oss-cn-shenzhen.aliyuncs.com/devops/gitlab-runner.tar.gz'

sudo su <<EOF
wget -P /tmp/ $SW_URL
cd /tmp && tar xzvf  gitlab-runner.tar.gz
mv /tmp/gitlab-runner-linux-386 /usr/local/bin/gitlab-runner
chmod +x /usr/local/bin/gitlab-runner
/usr/local/bin/gitlab-runner install --user=devops --working-directory=/home/devops
/usr/local/bin/gitlab-runner start
echo "Pls run below to register:\n /usr/local/bin/gitlab-runner register"
EOF

## find registeration info from your project hosted at https://gitlab.com/