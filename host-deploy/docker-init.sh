#!/bin/bash

host='devops.wicc.me'

DOCKER_ALIAS="
#docker_utils_alias
alias  dimg='docker images'
alias  dps='docker ps'
alias  dpsa='docker ps -a'
alias  drm='docker rm'
alias  drmi='docker rmi'
alias  dex='docker exec -it $1'
alias  dstart='docker start'
alias  dstop='docker stop'
alias  drestart='docker restart'
alias  dstats='docker stats --no-stream'
alias  dlogs='docker logs'
alias  dtag='docker tag'
alias  drenm='docker rename'
alias  dpull='docker pull'
alias  dpush='docker push'
alias  dsch='docker search'"

REGMIRRORS='{ "registry-mirrors":["https://zj674o4t.mirror.aliyuncs.com"] }'

HOSTADD="
10.24.165.35	hn-admin
120.76.100.121	hn-admin-pub"


docker_install(){
	yum install -y docker
	echo  $REGMIRRORS  > /etc/docker/daemon.json
	systemctl  restart  docker
	chmod a+rw /var/run/docker.sock
}

docker_utils(){
    D_FILE='/etc/docker/.bashrc_docker'
#	[ ! -f $D_FILE ] && wget -P /etc/docker  http://devops.ajy199.cn/src/soft_setup/docker_utils/.bashrc_docker
	[ ! -f $D_FILE ] && wget -P /etc/docker  http://devops.wicc.me/soft_setup/docker_utils/.bashrc_docker
	echo "[ -f /etc/docker/.bashrc_docker ] && . /etc/docker/.bashrc_docker" >> ~/.bashrc
	echo "[ -f /etc/docker/.bashrc_docker ] && . /etc/docker/.bashrc_docker" >> /home/richardchen/.bashrc
	echo "[ -f /etc/docker/.bashrc_docker ] && . /etc/docker/.bashrc_docker" >> /home/richardchen/.zshrc
	echo  "$DOCKER_ALIAS" >> ~/.bashrc
	echo  "$DOCKER_ALIAS" >> /home/richardchen/.bashrc
	echo  "$DOCKER_ALIAS" >> /home/richardchen/.zshrc
	source  ~/.bashrc
}

docker_images_load(){
    DIMG_FILE='/tmp/dimg/centos7_openjdk1.8_v1.2.dimg'
    if [ ! -f $DIMG_FILE ]; then
      wget -P /tmp/dimg  http://docker.wicc.me/centos7_openjdk1.8_v1.2.dimg
    fi
	
	docker load < $DIMG_FILE
}

utils_install(){
	yum install -y nfs-utils nfs-utils-lib nfs4-acl-tools rpcbind util-linux tmux htop
	iptables -I INPUT 1 -i docker0 -j ACCEPT
}

nfs_client_help(){
	#echo  "$HOSTADD"  >> /etc/hosts
	echo  ""
	echo  "		è¯·åœ¨ hn-admin ä¸»æœºä¸Šç¼–è¾‘ NFS SERVER é…ç½®æ–‡ä»¶ï¼šsudo vi /etc/exports "
	echo  "		åœ¨ /etc/exports ä¸­æ·»åŠ  NFSå®¢æˆ·ç«¯å…±äº«ç›®å½•ï¼š/data NFS_client_IP(rw,async,fsid=0,no_root_squash)Â "
	echo  "		NFSå®¢æˆ·ç«¯æŒ‚è½½ NFSå…±äº«ç›®å½•å‘½ä»¤ï¼šsudo mount  -t nfs hn-admin:/data  /data "
	echo  ""
}

docker_install
docker_utils
# docker_images_load
utils_install
#nfs_client_help
