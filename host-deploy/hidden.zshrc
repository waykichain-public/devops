export ZSH="/home/richardchen/.oh-my-zsh"

ZSH_THEME='mytheme'

alias rm='rm -i'
alias l='ls -la'
alias c='clear'
alias dex='sh /data/sbin/dex.sh'
alias dps='docker ps'
alias dpsa='docker ps -a'
alias dimg='docker images'
alias sc='systemctl'
alias dex='docker exec -it $1'

plugins=(
  git
)

source $ZSH/oh-my-zsh.sh
