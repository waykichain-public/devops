#!/bin/bash
#
# Usage: install-http2https-plugin.sh
#
# get into Kong container and execute the following scripts
#
#
cd /tmp
git clone https://github.com/HappyValleyIO/kong-http-to-https-redirect.git
cd /tmp/kong-http-to-https-redirect && luarocks install *.rockspec