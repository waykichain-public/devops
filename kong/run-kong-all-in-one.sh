#!/bin/bash
#
# Usage: run docker kong-db, kong, konga
#

docker run -d --name kong-database -p 5432:5432 \
  -e "POSTGRES_USER=kong" \
  -e "POSTGRES_DB=kong" \
  -e "POSTGRES_PASSWORD=bw_w1cc_1000" \
  postgres:9.6

echo "sleep 10 sec..." && sleep 10

docker run --privileged --rm \
  --link kong-database:kong-database \
  -e "KONG_DATABASE=postgres" \
  -e "KONG_PG_HOST=kong-database" \
  -e "KONG_PG_PASSWORD=bw_w1cc_1000" \
  kong kong migrations bootstrap

echo "sleep 10 sec..." && sleep 10

docker run -d --privileged --name kong \
  --link kong-database:kong-database \
  -e "KONG_DATABASE=postgres" \
  -e "KONG_PG_HOST=kong-database" \
  -e "KONG_PG_PASSWORD=bw_w1cc_1000" \
  -e "KONG_PROXY_ACCESS_LOG=/dev/stdout" \
  -e "KONG_ADMIN_ACCESS_LOG=/dev/stdout" \
  -e "KONG_PROXY_ERROR_LOG=/dev/stderr" \
  -e "KONG_ADMIN_ERROR_LOG=/dev/stderr" \
  -e "KONG_ADMIN_LISTEN=0.0.0.0:8082, 0.0.0.0:8083 ssl" \
  -p 80:8000 \
  -p 443:8443 \
  -p 1801:8082 \
  kong

docker run -d -p 9087:1337 --link kong-database:kong-database \
   -e "TOKEN_SECRET=somerandomstring" \
   -e "DB_ADAPTER=postgres" \
   -e "DB_HOST=kong-database" \
   -e "DB_USER=kong" \
   -e "DB_PASSWORD=bw_w1cc_1000" \
   -e "DB_PG_SCHEMA=konga-schema" --name kong-admin pantsel/konga