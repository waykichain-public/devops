1. create nginx dir `mkdir -p ~/nginx`
2. create `docker-compose.yml`
3. create conf.d dir `mkdir -p ~/nginx/conf.d`
4. add web.conf_ & webs.conf_
5. enable web.conf by removing "_"
6. run `docker-compose up -d` to retrieve certificate (remember to update domain name within the files)
7. disable web.conf and enable webs.conf
8. run `docker-compose up -d` again to finish everything
9. test https access to the domain from outside the server 